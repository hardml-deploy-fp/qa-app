FROM ubuntu:18.04 

RUN apt-get update && apt-get install -y --no-install-recommends \
         build-essential \ 
         git \
         curl \
         ca-certificates \
         libjpeg-dev \
         libpng-dev && \
     rm -rf /var/lib/apt/lists/*

RUN curl -L -o ~/miniconda.sh -O  https://repo.continuum.io/miniconda/Miniconda3-latest-Linux-x86_64.sh  && \
     chmod +x ~/miniconda.sh && \
     ~/miniconda.sh -b -p /opt/conda && \     
     rm ~/miniconda.sh && \
     /opt/conda/bin/conda install conda-build && \
     /opt/conda/bin/conda clean -ya 

ENV PATH /opt/conda/bin:$PATH


RUN conda install requests numpy flask  && /opt/conda/bin/conda clean -ya
RUN conda install -c conda-forge faiss gunicorn

WORKDIR /workspace

COPY . /workspace/qa-app

RUN chmod -R a+w /workspace

ARG model_version
ENV model_version=$model_version

ENV FLASK_APP=/workspace/qa-app/qa_app.py

WORKDIR /workspace/qa-app



