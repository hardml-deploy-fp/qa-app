import faiss 
import requests
import json
import pickle
import os

import numpy as np 

from flask import Flask, request, jsonify

version = os.environ['model_version']

app = Flask(__name__)

url_to_encoder = "http://95.217.183.140:8501/v1/models/universal-sentence-encoder-qa_3:predict"

path_to_cluster_template = '{path_to_index}/cluster_{cluster_num}'
path_to_index = f'/var/models/faiss_flat_index/{version}'

with open(f'{path_to_index}/clusters_centers_use_dg{version}.pkl', 'rb') as cluster_centers_file:
    clusters_centers = pickle.load(cluster_centers_file)

@app.route("/")
def print_version():
    return jsonify({'version':version})

@app.route("/search_duplicates", methods=['POST'])
def search_duplicates():
    question = request.json['question']

    data = json.dumps({"signature_name":"question_encoder", "inputs":[question]})

    embedding = np.array(requests.post(url_to_encoder, data=data).json()['outputs'][0]).reshape([1, -1]).astype(np.float32)
    
    distances = np.array([np.linalg.norm(cluster - embedding) for cluster in clusters_centers.values()])
    min_dist_cluster = np.argmin(distances)

    path_to_min_clust = path_to_cluster_template.format(path_to_index=path_to_index, cluster_num=min_dist_cluster)

    min_index = faiss.read_index(f'{path_to_min_clust}/index')
    
    with open(f'{path_to_min_clust}/index_to_document.pkl', 'rb') as index_to_docs_file:
        index_to_docs = pickle.load(index_to_docs_file)
        

    _, I = min_index.search(embedding, 5)

    duplicate_docs = [index_to_docs[i] for i in list(*I)]

    return jsonify({'duplicated_questions':duplicate_docs})

    